//LIBRERIAS
  //INPUT
    //SENSORES
      //AM2301 TEMPERATURA Y HUMEDAD
      #include <dht.h>
      dht DHT;

//CONSTANTES
  //INPUT
    //SENSORES
      //AM2301
      #define DHT21_PIN 2 //Define el tipo de sensor de humedad/temperatura y el pin al que va conectado
      //SMOOTHING VALORES 
      const int numReadings = 20; // Define el numero de lecturas que hace para el Smoothing de los valores de Temperatura y Humedad AJUSTABLE. Habria que darle un delay de 60 segundos al sistema para que mida bien.
  //OUTPUT
    // RELES
      const int rele1 = 3; // Define el pin digital que accionará el relé1
//VARIABLES
  //INPUT
    //SENSORES
    float humAM2302; // Variable de humedad del sensor 
    float tempAM2302; // Variable temperatura del sensor
    
      //Smoothing (hace que los valores recibidos por el sensor sean mas fijos)
        //Humedad ambiental
          int readingshum[numReadings]; // Crea un array con el numero de posiciones que le diga numReadings para almacenar valores sobre los que hacer una media
          int readIndexhum = 0; // El indice de la actual lectura
          float totalhum = 0;  // El total actual (corriendo)
          float averagehum = 0; // La media
        //Temperatura
          int readingstemp[numReadings]; // Crea un array con el numero de posiciones que le diga numReadings para almacenar valores sobre los que hacer una media
          int readIndextemp = 0; // El indice de la actual lectura
          float totaltemp = 0;  // El total actual (corriendo)
          float averagetemp = 0; // La media
        //Humedad tierra  
          int htierraPin = A0; // PIN en el que esta conectado el sensor de humedad de tierra
                // you can adjust the threshold value
          int umbralHtierra  = 800; // Dato que ajusta el umbral del sensor. AJUSTABLE

  //OUTPUT  
    //OTROS
    int LEDpin =13; //Pin para pruebas
    //MILLIS (DELAYS)
    long previousMillisHUMTEMP = 0;//Pone a millis a 0 al encender. Necesario para el correcto funcionamiento de millis. Ver en como no usar delay (documentacion)
    long intervaHUMTEMP = 2000 ; //Delay asignado para la visualización de la Humedad y Temperatura en el Monitor. AJUSTABLE
    long previousMillisHUMTIERR = 0;
    long intervaHUMTIERR = 2000 ; //Delay asignado para la visualización de la Humedad de la Tierra en el Monitor. AJUSTABLE
    long previousMillisrele1 = 0;
    long intervarele1 = 2000 ;//Delay asignado para activación del rele. AJUSTABLE


void setup() {
  // put your setup code here, to run once:
//INPUT
  //SENSORES
    //Humedad Tierra
      pinMode(htierraPin, INPUT); //Define el PIN del sensor de humedad de tierra como de entrada
    //Smoothing
      //Humedad ambiental y Temperatura
      for (int thisReading = 0; thisReading < numReadings; thisReading++) {
      readingshum[thisReading] = 0;
      readingstemp[thisReading] = 0;
      } // Cambia todos las posiciones de los arrays readingshum y readingstemp a 0. Los "inicializa"
//OUTPUT
  //RELES
    pinMode(rele1, OUTPUT); //Define el pin del rele1 como pin de salida
  //OTROS
    Serial.begin(9600); // Para comunicar con el monitor serial
    pinMode (LEDpin, OUTPUT); //Define el LEDpin como pin de salida - No tiene ninguna utilidad especifica, solo para pruebas 

}

void loop() {
  // put your main code here, to run repeatedly:
//INPUT  
  // SENSORES

    //TEMPERATURA Y HUMEDAD AM2302
    
    tempAM2302 = DHT. temperature; // Lee los datos de humedad y temperatura y los almacena.
  
      //Smoothing de Humedad
        int chk = DHT.read21(DHT21_PIN);
        totalhum = totalhum - readingshum[readIndexhum]; // Resta la ultima lectura
        readingshum[readIndexhum]= humAM2302 = DHT.humidity; // Lee los datos del sensor (humedad)
        
        totalhum = totalhum + readingshum[readIndexhum]; // Añade la lectura al total
        readIndexhum = readIndexhum + 1; // Avanza a la siguiente posicion del array
        if (readIndexhum >= numReadings){
          readIndexhum = 0;
          } // Si llegamos al final del array...
    
        averagehum = totalhum / numReadings; // Calculo de la media
  
      //Smoothing de Temperatura
              totaltemp = totaltemp - readingstemp[readIndextemp]; // Resta la ultima lectura
        readingstemp[readIndextemp]= tempAM2302 = DHT.temperature; // Lee los datos del sensor (temperatura)
              totaltemp = totaltemp + readingstemp[readIndextemp]; // Añade la lectura al total
        readIndextemp = readIndextemp + 1; // Avanza a la siguiente posicion del array
        if (readIndextemp >= numReadings){
          readIndextemp = 0;
          } // Si llegamos al final del array...
    
        averagetemp = totaltemp / numReadings; // Calculo de la media

      // HUMEDAD TIERRA

        int valorSensorHT = analogRead(htierraPin); //Declara la variable valorSensorHT y lee el valor que da el sensor de humedad en suelo

//OUTPUT

  //CONSECUENCIAS DE LOS VALORES EN MENSAJES AL MONITOR:
    //HUMEDAD AMBIENTAL Y TEMPERATURA
     unsigned long currentMillisHUMTEMP = millis();
     if(currentMillisHUMTEMP - previousMillisHUMTEMP > intervaHUMTEMP){//(ver documentacion "como no usar delay") DELAY EN MILLIS
      previousMillisHUMTEMP = currentMillisHUMTEMP;
      Serial.print("Humidity: "); 
         Serial.print(averagehum);
         Serial.print(" %\t");
         Serial.print("Temperature: "); 
         Serial.print(averagetemp);
         Serial.println(" *C");
     }
     
    
      
  
     //HUMEDAD EN TIERRA

     unsigned long currentMillisHUMTIERR = millis();
     if(currentMillisHUMTIERR - previousMillisHUMTIERR > intervaHUMTIERR){ //(ver documentacion "como no usar delay") DELAY EN MILLIS
      previousMillisHUMTIERR = currentMillisHUMTIERR;
        Serial.print(valorSensorHT); // Manda al monitor el valor que da el sensor de humedad en suelo
          if(valorSensorHT < umbralHtierra){ // Si el valor que da el sensor de humedad en suelo es menor a el umbral de humedad en tierra que mas arriba hemos declarado...
            Serial.println(" - No necesita agua");
            
          }
          else { // En caso contrario...
            Serial.println(" - Necesita agua");
            
          }
     }
        
        
  // SERVOS

  // RELES
  unsigned long currentMillisrele1 = millis(); //Crea una variable que almacena el valor de millis (un contador de milisegundos de arduino)(ver documentacion "como no usar delay")
     if(currentMillisrele1 - previousMillisrele1 > intervarele1){ //Si el valor de millis de ahora menos el valor del millis previo (es cero, asignado arriba) es mayor que el intervalo entonces hacer...
      previousMillisrele1 = currentMillisrele1; // el millis previo es ahora
       digitalWrite(rele1, LOW);
                                
          }
          else{
           digitalWrite(rele1, HIGH); //Enciende y apaga el rele1 con un delay de 2000 ms //En caso contrario enciende el rele
          }
     
    
  // LOGICA

  // OTROS
}
